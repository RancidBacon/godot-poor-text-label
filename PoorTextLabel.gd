#
# >> PoorTextLabel <<
#
# GDScript proof of concept to enable font size to be set via bbcode in RichTextLabel.
#
# Example text: "[color=#ff00ff]hello [font size=32]there[/font][/color] Godot"
#
# Limitations of this implementation include:
#
#  * Need to set the text via e.g. `$"PoorTextLabel".set_bbcode(...)`
#
#  * Sized text can't be between other bbcode open/close tag pairs.
#    (But you can work around this by duplicating the tags before/inside/after
#    the `[font size=]` tag pair--or sometimes by just not closing a tag.)
#    (This is because `append_bbcode()` function doesn't appear to like bbcode fragments.)
#
#  * Strikethrough lines may not be drawn at correct height. (Appears to be Godot bug.)
#
#  * Only applies to "normal" text currently.
#
#  * Not robust against errors.
#
#  Brought to you in 2019 by RancidBacon.com with an MIT License.
#

extends RichTextLabel

class_name PoorTextLabel

tool

func _handle_next_font_size_tag(value: String) -> String:

    var head: String
    var tail: String

    var result = value.split("[font size=", true, 1)

    head = result[0]
    tail = result[1]

    result = tail.split("]", true, 1)

    var font_size: int = int(result[0])
    tail = result[1]

    result = tail.split("[/font]", true, 1)

    var middle: String = result[0]

    tail = result[1]


    var base_font: DynamicFont = self.get("custom_fonts/normal_font")
    var new_font = base_font.duplicate()
    new_font.size = font_size


    # warning-ignore:return_value_discarded
    .append_bbcode(head)
    .push_font(new_font)
    # warning-ignore:return_value_discarded
    .append_bbcode(middle)
    .pop()

    return tail


# Alas, we can't override setters AFAICT...
func set_bbcode(value: String):

    var tail: String

    tail = value

    .clear()

    while (tail.find("[font size=") != -1):

        tail = self._handle_next_font_size_tag(tail)

    # warning-ignore:return_value_discarded
    .append_bbcode(tail)
