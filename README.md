### `PoorTextLabel`

GDScript proof of concept to enable font size to be set via bbcode in RichTextLabel.

![](godot-poor-text-label-demo.png)

The above example was created with:

    $"PoorTextLabel".set_bbcode("[center]\nHello [font size=48][color=#ffc0ff]there[/color][/font] Godot, my friend.\nI've been [font size=28][color=#c0c0ff][u]waiting[/u][/color][/font] for [font size=48][color=#ffc0ff]you[/color][/font].")

See the file `PoorTextLabel.gd` for more details.