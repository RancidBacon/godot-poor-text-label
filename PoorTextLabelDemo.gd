extends Node

func _ready():

    var new_font: DynamicFont = DynamicFont.new()
    new_font.font_data = load("res://fonts/barlow/Barlow-Bold.ttf")
    new_font.size = 16
    $"PoorTextLabel".set("custom_fonts/normal_font", new_font)

    $"PoorTextLabel".set_bbcode("[center]\nHello [font size=48][color=#ffc0ff]there[/color][/font] Godot, my friend.\nI've been [font size=28][color=#c0c0ff][u]waiting[/u][/color][/font] for [font size=48][color=#ffc0ff]you[/color][/font].")